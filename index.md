---
layout: content
---

# John

[https://linuxwho.gitlab.io/blog](https://gitlab.com/linuxwho/blog)

### About

My name is John Smith, I am a college student in China. As a FOSS enthusiasm and linux user, I love free software, my computer
runs upon linux which is [free software], Currently I am learning Algorithm and computer network by myself. 

### Distros

- [x] ArchLinux(most of time)
- [x] Debian(on my server)
- [x] fedora(Sometimes I test it for fun)
- [x] gentoo(compiled it twice)

### Programming languages

- [x] C(oldest and my favorite)
- [x] Python & Bash(scripting)
- [x] C++
- [x] willing to learn anything else except java

### Screenshots

![Screenshot]({{ site.baseurl }}images/screenshot/01.png)

![Screenshot]({{ site.baseurl }}images/screenshot/02.png)


